package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.twilio.interview.cloudinfrastructure.model.*;
import com.twilio.interview.cloudinfrastructure.model.utils.TextUtils;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

class HostImpl implements Host {
    private static final int SHUTDOWN_TIMEOUT = 10000;
    private final String id = UUID.randomUUID().toString();
    private final HostType hostType;
    private final Group groupType;
    private final LoadBalancer loadBalancer;
    private volatile ExecutorService worker;
    private volatile HostState state;
    private volatile HostStateChangedListener listener;

    HostImpl(@NotNull HostType hostType,
             @NotNull Group group,
             @NotNull LoadBalancer loadBalancer) {
        this.hostType = hostType;
        this.groupType = group;
        this.loadBalancer = loadBalancer;
        this.state = HostState.CREATED;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return hostType.getType();
    }

    @Override
    public String getDescription() {
        return hostType.getDescription();
    }

    @Override
    public HostSize getSize() {
        return groupType.getSize();
    }

    @Override
    public HostState getState() {
        return state;
    }

    @Override
    public boolean isActive() {
        return state == HostState.RUNNING && loadBalancer.isActive(this);
    }

    @Override
    public void boot() throws IllegalStateException {
        goToState(HostState.BOOTING);
    }

    @Override
    public void activate() throws IllegalStateException {
        if (state != HostState.RUNNING) {
            throw new IllegalStateException("The host must be in running state before activation");
        }
        loadBalancer.addHost(this);
    }

    @Override
    public void deactivate() throws IllegalStateException {
        loadBalancer.removeHost(this);
    }

    @Override
    public void shutdown() throws IllegalStateException {
        if (isActive()) {
            throw new IllegalStateException("The host must be deactivated before shutting down");
        }
        goToState(HostState.SHUTTING_DOWN);
    }

    @Override
    public void setStateChangedListener(@Nullable HostStateChangedListener listener) {
        this.listener = listener;
    }


    @Override
    public void onRequest(@NotNull String type,
                          @NotNull String request,
                          @Nullable LoadBalancer.ResultListener listener) {
        if (!TextUtils.equals(hostType.getType(), type)) {
            throw new IllegalArgumentException("The request doesn't match the host type");
        }
        if (state != HostState.RUNNING) {
            throw new IllegalStateException("Host must be in running state to process requests");
        }
        try {
            getWorker().submit((Runnable) () -> {
                System.out.println();
                System.out.println(String.format("Process request %s on host %s (%s)", request, getId(), getType()));
                listener.onResult(type + request);
            });
        } catch (RejectedExecutionException ex) {
            ex.printStackTrace();
            listener.onError(LoadBalancer.ErrorType.SERVICE_NOT_AVAILABLE);
        }
    }

    private synchronized void goToState(HostState nextState) {
        if (state == nextState) {
            return;
        }
        switch (nextState) {
            case CREATED: {
                throw new IllegalStateException();
            }
            case BOOTING: {
                if (state != HostState.CREATED && state != HostState.SHUTDOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onBooting();
                break;
            }
            case CONFIGURING: {
                if (state != HostState.BOOTING) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onConfiguring();
                break;
            }
            case RUNNING: {
                if (state != HostState.CONFIGURING) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onRunning();
                break;
            }
            case SHUTTING_DOWN: {
                if (state == HostState.SHUTDOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onShuttingDown();
                break;
            }
            case SHUTDOWN: {
                if (state != HostState.SHUTTING_DOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onShutDown();
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown state found");
            }
        }
    }

    private void setStateInternal(HostState nextState) {
        HostState current = state;
        state = nextState;
        HostStateChangedListener currentListener = listener;
        if (currentListener != null) {
            currentListener.onStateChanged(state);
        }
        logTransition(current, nextState);
    }

    private void onShutDown() {
        logCurrentState("shutdown");
    }

    private void onShuttingDown() {
        logCurrentState("shutting down started");
        for (int i = 0; i < 10; ++i) {
            System.out.print(".");
        }
        System.out.println();
        final ExecutorService currentWorker = worker;
        if (currentWorker == null) {
            logCurrentState("shutting down completed");
            goToState(HostState.SHUTDOWN);
        } else {
            Executors.newCachedThreadPool().submit((Runnable) () -> {
                logCurrentState("waiting for previously submitted tasks to be completed");
                currentWorker.shutdown();
                try {
                    currentWorker.awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    resetWorker();
                    logCurrentState("shutting down completed");
                    goToState(HostState.SHUTDOWN);
                }
            });
        }
    }

    private void onRunning() {
        logCurrentState("ready to process requests");
    }

    private void onConfiguring() {
        logCurrentState("configure started");
        for (int i = 0; i < 10; ++i) {
            System.out.print(".");
        }
        System.out.println();
        logCurrentState("configure completed");
        goToState(HostState.RUNNING);
    }

    private void onBooting() {
        logCurrentState("boot started");
        getWorker();
        for (int i = 0; i < 20; ++i) {
            System.out.print(".");
        }
        System.out.println();
        logCurrentState("boot completed");
        goToState(HostState.CONFIGURING);
    }

    private void logCurrentState(String action) {
        System.out.println(String.format(
                "Host %s (%s) : %s",
                getId(),
                getType(),
                action
                )
        );
    }

    private void logTransition(HostState current, HostState next) {
        System.out.println(String.format(
                "Host %s (%s) switched from %s to %s",
                getId(),
                getType(),
                current,
                next
            )
        );
    }


    private void raiseErrorTransition(HostState current, HostState next) throws IllegalStateException {
        throw new IllegalStateException(String.format(
                "Host %s (%s) failed to switch from %s to %s",
                getId(),
                getType(),
                current,
                next
        ));
    }

    private ExecutorService getWorker() {
        if (worker == null) {
            synchronized (this) {
                if (worker == null) {
                    worker = Executors.newSingleThreadExecutor(r -> {
                        Thread thread = new Thread(r);
                        thread.setName(String.format("Host %s (%s)", getId(), getType()));
                        return thread;
                    });
                }
            }
        }
        return worker;
    }

    private void resetWorker() {
        worker = null;
    }

    @Override
    public String toString() {
        return "HostImpl{" +
                "id='" + id + '\'' +
                ", hostType=" + hostType +
                ", state=" + state +
                '}';
    }
}
