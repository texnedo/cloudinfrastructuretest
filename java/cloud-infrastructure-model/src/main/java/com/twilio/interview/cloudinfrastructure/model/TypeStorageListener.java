package com.twilio.interview.cloudinfrastructure.model;

public interface TypeStorageListener<T> {
    void onRemoved(T item);
}
