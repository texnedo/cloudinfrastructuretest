package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.twilio.interview.cloudinfrastructure.model.HostType;
import com.twilio.interview.cloudinfrastructure.model.SerializableType;
import com.twilio.interview.cloudinfrastructure.model.StorageBundle;
import com.twilio.interview.cloudinfrastructure.model.TypeStorage;
import com.twilio.interview.cloudinfrastructure.model.utils.TextUtils;

import java.io.IOException;
import java.util.UUID;

public class HostTypeImpl implements HostType {
    private String id;
    private String type;
    private String description;
    private TypeStorage storage;

    public HostTypeImpl(String type, String description) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.description = description;
    }

    HostTypeImpl(@NotNull TypeStorage storage) {
        this.storage = storage;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        boolean change = !TextUtils.equals(this.description, description);
        if (change) {
            this.description = description;
            update();
        }
    }

    @Override
    public void remove() {
        if (storage == null) {
            throw new IllegalStateException("Type must be attached to a storage");
        }
        try {
            storage.remove(getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void setStorage(TypeStorage storage) {
        this.storage = storage;
    }

    @Override
    public void serialize(@NotNull StorageBundle bundle) {
        bundle.put("id", id);
        bundle.put("type", type);
        bundle.put("description", description);
    }

    @Override
    public void unSerialize(@NotNull StorageBundle bundle) {
        id = bundle.getString("id");
        if (TextUtils.isEmpty(id)) {
            throw new IllegalArgumentException();
        }
        type = bundle.getString("type");
        if (TextUtils.isEmpty(type)) {
            throw new IllegalArgumentException();
        }
        description = bundle.getString("description");
    }

    protected void update() {
        if (storage != null) {
            try {
                storage.update();
            } catch (IOException ex) {
                System.err.println("Failed to update " + toString());
                ex.printStackTrace();
            }
        }
    }

    @Nullable
    protected SerializableType getById(@NotNull String id) {
        if (storage != null) {
            try {
                return storage.get(id);
            } catch (IOException ex) {
                System.err.println("Failed to get item by id " + id);
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "HostTypeImpl{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
