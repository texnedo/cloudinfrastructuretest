package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

public interface LoadBalancer {
    void onRequest(@NotNull String type,
                   @NotNull String request,
                   @Nullable ResultListener listener) throws IllegalStateException;

    void addHost(@NotNull Host host) throws IllegalStateException;

    void removeHost(@NotNull Host host) throws IllegalStateException;

    void removeHost(@NotNull String id)  throws IllegalStateException;

    boolean isActive(@NotNull Host host);

    boolean isActive(@NotNull String id);

    interface ResultListener {
        void onResult(@NotNull String result);

        void onError(ErrorType errorType);
    }

    enum ErrorType {
        GENERAL_ERROR,
        SERVICE_NOT_AVAILABLE
    }
}
