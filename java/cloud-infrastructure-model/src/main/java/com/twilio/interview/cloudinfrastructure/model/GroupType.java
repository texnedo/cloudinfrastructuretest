package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;

import java.util.Collection;

/**
 * Interface {@link GroupType} defines the contract implemented by the host
 * group type objects.
 */
public interface GroupType extends HostType {
    /**
     * Returns host (instance) size for the group.
     * 
     * @return  host size
     */
    HostSize getSize();

    /**
     * Returns the number of hosts (instances) for the group.
     * 
     * @return  host count
     */
    int getHostCount();

    /**
     * Returns the flag indicating whether host group should be activated
     * (brought into the load balancer) after successful boot or not.
     * 
     * @return  activate on boot flag
     */
    boolean isActiveOnBoot();

    /**
     * Returns the collection of {@link GroupType}s the group depends on.
     * 
     * @return  group dependencies
     */
    Collection<GroupType> getDependencies();

    void addDependency(@NotNull GroupType groupType);

    boolean hasDependency(@NotNull String id);
}
