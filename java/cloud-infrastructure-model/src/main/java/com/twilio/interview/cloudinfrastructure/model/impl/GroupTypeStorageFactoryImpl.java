package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.GroupType;
import com.twilio.interview.cloudinfrastructure.model.StorageBundle;
import com.twilio.interview.cloudinfrastructure.model.TypeStorage;
import com.twilio.interview.cloudinfrastructure.model.TypeStorageFactory;

class GroupTypeStorageFactoryImpl implements TypeStorageFactory<GroupType> {
    @Override
    public GroupType create(@NotNull TypeStorage<GroupType> storage, @NotNull StorageBundle bundle) {
        GroupTypeImpl groupType = new GroupTypeImpl(storage);
        groupType.unSerialize(bundle);
        return groupType;
    }
}
