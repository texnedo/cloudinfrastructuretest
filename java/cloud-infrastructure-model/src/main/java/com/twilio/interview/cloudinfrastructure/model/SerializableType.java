package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;

public interface SerializableType {
    void serialize(@NotNull StorageBundle bundle);

    void unSerialize(@NotNull StorageBundle bundle);

    String getId();

    String getType();

    void setStorage(@NotNull TypeStorage storage);
}
