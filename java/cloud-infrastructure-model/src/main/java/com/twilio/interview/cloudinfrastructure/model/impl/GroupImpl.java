package com.twilio.interview.cloudinfrastructure.model.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.*;

class GroupImpl implements Group {
    public static final int SHUTDOWN_TIMEOUT = 30 * 1000;
    private final String id = UUID.randomUUID().toString();
    private final GroupType groupType;
    private final HostFactory hostFactory;
    private HashMap<String, Host> hosts;
    private GroupState state;

    GroupImpl(@NotNull GroupType groupType,
              @NotNull HostFactory hostFactory) {
        this.groupType = groupType;
        this.hostFactory = hostFactory;
        this.state = GroupState.CREATED;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return groupType.getType();
    }

    @Override
    public String getTypeId() {
        return groupType.getId();
    }

    @Override
    public String getDescription() {
        return groupType.getDescription();
    }

    @Override
    public HostSize getSize() {
        return groupType.getSize();
    }

    @Override
    public GroupState getState() {
        return state;
    }

    @Override
    public Collection<Host> getHosts() {
        if (hosts == null) {
            hosts = new HashMap<>();
            for (int i = 0; i < groupType.getHostCount(); ++i) {
                Host host = hostFactory.create(this);
                hosts.put(host.getId(), host);
            }
        }
        return hosts.values();
    }

    @Override
    public void boot() throws IllegalStateException {
        goToState(GroupState.BOOTING);
    }

    @Override
    public void shutdown() throws IllegalStateException {
        goToState(GroupState.SHUTTING_DOWN);
    }

    @Override
    public void activate() {
        for (Host host : getHosts()) {
            host.activate();
        }
    }

    @Override
    public boolean isActive() {
        for (Host host : getHosts()) {
            if (host.isActive()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void deactivate() {
        for (Host host : getHosts()) {
            host.deactivate();
        }
    }

    @Override
    public Host getHost(@NotNull String id) {
        if (hosts != null) {
            return hosts.get(id);
        }
        return null;
    }

    private void goToState(GroupState nextState) {
        if (state == nextState) {
            return;
        }
        switch (nextState) {
            case CREATED: {
                throw new IllegalStateException();
            }
            case BOOTING: {
                if (state != GroupState.CREATED && state != GroupState.SHUTDOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onBooting();
                break;
            }
            case RUNNING: {
                if (state != GroupState.BOOTING) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onRunning();
                break;
            }
            case SHUTTING_DOWN: {
                if (state == GroupState.SHUTDOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onShuttingDown();
                break;
            }
            case SHUTDOWN: {
                if (state != GroupState.SHUTTING_DOWN) {
                    raiseErrorTransition(state, nextState);
                }
                setStateInternal(nextState);
                onShutDown();
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown state found");
            }
        }
    }

    private void setStateInternal(GroupState nextState) {
        GroupState current = state;
        state = nextState;
        logTransition(current, nextState);
    }

    private void onShutDown() {
        logCurrentState("shutdown");
    }

    private void onShuttingDown() {
        logCurrentState("shutting down started");
        Collection<Host> hostsToShutdown = getHosts();
        CountDownLatch latch = new CountDownLatch(hostsToShutdown.size());
        for (Host host : hostsToShutdown) {
            host.deactivate();
            host.setStateChangedListener(hostState -> {
                logCurrentState(
                        String.format(
                                "Host %s (%s) went to state %s",
                                host.getId(),
                                getType(),
                                hostState
                        )
                );
                if (hostState == HostState.SHUTDOWN) {
                    latch.countDown();
                    host.setStateChangedListener(null);
                }
            });
            host.shutdown();
        }
        try {
            latch.await(SHUTDOWN_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            logCurrentState("shutting down completed");
            goToState(GroupState.SHUTDOWN);
        }
    }

    private void onRunning() {
        logCurrentState("ready to process requests");
        if (groupType.isActiveOnBoot()) {
            for (Host host : getHosts()) {
                host.activate();
            }
        }
    }

    private void onBooting() {
        logCurrentState("boot started");
        for (Host host : getHosts()) {
            host.boot();
        }
        logCurrentState("boot completed");
        goToState(GroupState.RUNNING);
    }

    private void logCurrentState(String action) {
        System.out.println(String.format(
                "Group %s (%s) : %s",
                getId(),
                getType(),
                action
                )
        );
    }

    private void logTransition(GroupState current, GroupState next) {
        System.out.println(String.format(
                "Group %s (%s) switched from %s to %s",
                getId(),
                getType(),
                current,
                next
                )
        );
    }


    private void raiseErrorTransition(GroupState current, GroupState next) throws IllegalStateException {
        throw new IllegalStateException(String.format(
                "Group %s (%s) failed to switch from %s to %s",
                getId(),
                getType(),
                current,
                next
        ));
    }

    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                ", groupType=" + groupType +
                ", hostFactory=" + hostFactory +
                ", hosts=" + hosts +
                ", state=" + state +
                '}';
    }
}
