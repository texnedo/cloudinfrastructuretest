package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.HostType;
import com.twilio.interview.cloudinfrastructure.model.StorageBundle;
import com.twilio.interview.cloudinfrastructure.model.TypeStorage;
import com.twilio.interview.cloudinfrastructure.model.TypeStorageFactory;

class HostTypeStorageFactoryImpl implements TypeStorageFactory<HostType> {
    @Override
    public HostType create(@NotNull TypeStorage<HostType> storage,
                           @NotNull StorageBundle bundle) {
        HostTypeImpl hostType = new HostTypeImpl(storage);
        hostType.unSerialize(bundle);
        return hostType;
    }
}
