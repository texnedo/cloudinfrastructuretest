package com.twilio.interview.cloudinfrastructure.model.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.*;

public class GroupTypeImpl extends HostTypeImpl implements GroupType {
    private HostSize hostSize;
    private int hostCount;
    private boolean isActiveOnBoot;
    private HashMap<String, GroupType> dependencies;
    private List<String> dependencyIds;

    public GroupTypeImpl(String type,
                         String description,
                         HostSize hostSize,
                         int hostCount,
                         boolean isActiveOnBoot) {
        super(type, description);
        this.hostSize = hostSize;
        this.hostCount = hostCount;
        this.isActiveOnBoot = isActiveOnBoot;
    }

    GroupTypeImpl(@NotNull TypeStorage storage) {
        super(storage);
    }

    @Override
    public void serialize(@NotNull StorageBundle bundle) {
        super.serialize(bundle);
        bundle.put("hostSize", hostSize.ordinal());
        bundle.put("hostCount", hostCount);
        bundle.put("isActiveOnBoot", isActiveOnBoot ? 1 : 0);
        if (dependencies != null) {
            List<String> groupTypes = new ArrayList<>(dependencies.size());
            for (GroupType type : dependencies.values()) {
                groupTypes.add(type.getId());
            }
            bundle.put("groupTypes", groupTypes);
        }
    }

    @Override
    public void unSerialize(@NotNull StorageBundle bundle) {
        super.unSerialize(bundle);
        hostSize = HostSize.values()[bundle.getInt("hostSize")];
        hostCount = bundle.getInt("hostCount");
        isActiveOnBoot = bundle.getInt("isActiveOnBoot") == 1;
        dependencyIds = bundle.getStringList("groupTypes");
    }

    @Override
    public HostSize getSize() {
        return hostSize;
    }

    @Override
    public int getHostCount() {
        return hostCount;
    }

    @Override
    public boolean isActiveOnBoot() {
        return isActiveOnBoot;
    }

    @Override
    public Collection<GroupType> getDependencies() {
        if (dependencies == null) {
            if (dependencyIds != null) {
                dependencies = new HashMap<>(dependencyIds.size());
                for (String id : dependencyIds) {
                    SerializableType item = getById(id);
                    if (item != null) {
                        dependencies.put(item.getId(), (GroupType) item);
                    }
                }
                dependencyIds = null;
            } else {
                dependencies = new HashMap<>();
            }
        }
        return dependencies.values();
    }

    @Override
    public void addDependency(@NotNull GroupType groupType) {
        if (dependencies == null) {
            dependencies = new HashMap<>();
        }
        GroupType previous = dependencies.put(groupType.getId(), groupType);
        if (previous == null) {
            update();
        }
    }

    @Override
    public boolean hasDependency(@NotNull String id) {
        if (dependencies == null) {
            return false;
        }
        return dependencies.containsKey(id);
    }
}
