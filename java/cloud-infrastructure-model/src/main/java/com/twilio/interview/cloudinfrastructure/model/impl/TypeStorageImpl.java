package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.twilio.interview.cloudinfrastructure.model.SerializableType;
import com.twilio.interview.cloudinfrastructure.model.StorageBundle;
import com.twilio.interview.cloudinfrastructure.model.TypeStorage;
import com.twilio.interview.cloudinfrastructure.model.TypeStorageFactory;

import javax.json.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

class TypeStorageImpl<T extends SerializableType> implements TypeStorage<T> {
    private final File storage;
    private final TypeStorageFactory<T> factory;
    private HashMap<String, T> items;
    private HashMap<String, T> itemByType;

    TypeStorageImpl(@NotNull String filePath,
                           @NotNull TypeStorageFactory<T> factory) {
        this.storage = new File(filePath);
        this.factory = factory;
    }

    @Override
    public Collection<T> get() throws IOException {
        load();
        return items.values();
    }

    @Override
    public T get(@NotNull String id) throws IOException {
        load();
        return items.get(id);
    }

    @Override
    public T getByType(@NotNull String type) throws IOException {
        load();
        return itemByType.get(type);
    }

    @Override
    public void add(@NotNull T type) throws IOException {
        load();
        type.setStorage(this);
        items.put(type.getId(), type);
        itemByType.put(type.getType(), type);
        save();
    }

    @Override
    public void remove(@NotNull T type) throws IOException {
        load();
        type.setStorage(null);
        items.remove(type.getId());
        itemByType.remove(type.getType());
        save();
    }

    @Override
    public void remove(@NotNull String id) throws IOException {
        load();
        T type = items.remove(id);
        if (type != null) {
            type.setStorage(null);
            itemByType.remove(type.getType());
        }
        save();
    }

    @Override
    public void update() throws IOException {
        load();
        save();
    }

    @Override
    public void clear() throws IOException {
        load();
        for (T type : items.values()) {
            type.setStorage(null);
        }
        items.clear();
        itemByType.clear();
        save();
    }

    private void save() throws FileNotFoundException {
        if (items == null) {
            return;
        }
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (T item : items.values()) {
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            item.serialize(new StorageBundle() {
                @Override
                public void put(@NotNull String key, @Nullable String value) {
                    objectBuilder.add(key, value);
                }

                @Override
                public void put(@NotNull String key, int value) {
                    objectBuilder.add(key, value);
                }

                @Override
                public void put(@NotNull String key, @NotNull List<String> values) {
                    JsonArrayBuilder builder = Json.createArrayBuilder();
                    for (String item : values) {
                        builder.add(item);
                    }
                    objectBuilder.add(key, builder);
                }

                @Override
                public String getString(@NotNull String key) {
                    throw new IllegalStateException();
                }

                @Override
                public int getInt(@NotNull String key) {
                    throw new IllegalStateException();
                }

                @Override
                public List<String> getStringList(@NotNull String key) {
                    return null;
                }
            });
            arrayBuilder.add(objectBuilder);
        }
        JsonWriter writer = Json.createWriter(new FileOutputStream(storage));
        writer.writeArray(arrayBuilder.build());
    }

    private void load() throws IOException {
        if (items != null) {
            return;
        }
        items = new HashMap<>();
        itemByType = new HashMap<>();
        if (storage.exists()) {
            try {
                JsonReader reader = Json.createReader(new FileInputStream(storage));
                JsonArray itemObjects = reader.readArray();
                for (int i = 0; i < itemObjects.size(); ++i) {
                    JsonObject itemObject = itemObjects.getJsonObject(i);
                    T item = factory.create(this, new StorageBundle() {
                        @Override
                        public void put(@NotNull String key, @Nullable String value) {
                            throw new IllegalStateException();
                        }

                        @Override
                        public void put(@NotNull String key, int value) {
                            throw new IllegalStateException();
                        }

                        @Override
                        public void put(@NotNull String key, @NotNull List<String> values) {
                            throw new IllegalStateException();
                        }

                        @Override
                        public String getString(@NotNull String key) {
                            return itemObject.getString(key);
                        }

                        @Override
                        public int getInt(@NotNull String key) {
                            return itemObject.getInt(key);
                        }

                        @Override
                        public List<String> getStringList(@NotNull String key) {
                            List<String> result = new ArrayList<>();
                            JsonArray array = itemObject.getJsonArray(key);
                            if (array != null) {
                                for(int i = 0; i < array.size(); ++i) {
                                    result.add(array.getString(i));
                                }
                            }
                            return result;
                        }
                    });
                    items.put(item.getId(), item);
                    itemByType.put(item.getType(), item);
                }
            } catch (IOException ex) {
                System.err.println("Failed to read from the file " + storage.getPath());
                throw ex;
            }
        }
     }
}
