package com.twilio.interview.cloudinfrastructure.model;

public interface HostStateChangedListener {
    void onStateChanged(HostState state);
}
