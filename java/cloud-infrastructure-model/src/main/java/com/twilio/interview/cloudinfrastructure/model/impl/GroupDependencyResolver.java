package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.GroupType;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

class GroupDependencyResolver {
    private final HashSet<GroupType> started = new HashSet<>();
    private final HashSet<GroupType> unvisited;

    GroupDependencyResolver(@NotNull Collection<GroupType> groupTypes) {
        this.unvisited = new HashSet<>(groupTypes);
    }

    @NotNull
    List<GroupType> getBootOrder() {
        LinkedList<GroupType> ordered = new LinkedList<>();
        while (!unvisited.isEmpty()) {
            GroupType current = unvisited.iterator().next();
            visit(current, ordered);
        }
        return ordered;
    }

    private void visit(@NotNull GroupType groupType, @NotNull List<GroupType> ordered) {
        if (started.contains(groupType)) {
            throw new IllegalArgumentException("A loop in dependencies detected");
        }
        if (!unvisited.contains(groupType)) {
            return;
        }
        started.add(groupType);
        for (GroupType item : groupType.getDependencies()) {
            visit(item, ordered);
        }
        started.remove(groupType);
        unvisited.remove(groupType);
        ordered.add(groupType);
    }
}
