package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.util.Collection;

public interface TypeStorage <T extends SerializableType> {
    @NotNull
    Collection<T> get() throws IOException;

    T get(@NotNull String id) throws IOException;

    T getByType(@NotNull String type) throws IOException;

    void add(@NotNull T type) throws IOException;

    void remove(@NotNull T type) throws IOException;

    void remove(@NotNull String id) throws IOException;

    void update() throws IOException;

    void clear() throws IOException;
}
