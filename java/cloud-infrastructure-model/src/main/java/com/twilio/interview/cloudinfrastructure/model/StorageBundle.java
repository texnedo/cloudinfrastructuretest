package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.List;

public interface StorageBundle {
    void put(@NotNull String key, @Nullable String value);

    void put(@NotNull String key, int value);

    void put(@NotNull String key, @NotNull List<String> values);

    String getString(@NotNull String key);

    int getInt(@NotNull String key);

    List<String> getStringList(@NotNull String key);
}
