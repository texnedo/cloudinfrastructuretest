package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;

public interface TypeStorageFactory <T extends SerializableType> {
    T create(
            @NotNull TypeStorage<T> storage,
             @NotNull StorageBundle bundle
    );
}
