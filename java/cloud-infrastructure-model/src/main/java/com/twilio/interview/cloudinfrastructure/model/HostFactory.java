package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;

public interface HostFactory {
    Host create(@NotNull Group group);
}
