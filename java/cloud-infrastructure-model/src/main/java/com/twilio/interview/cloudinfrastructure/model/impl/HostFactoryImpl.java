package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.*;

class HostFactoryImpl implements HostFactory {
    private final LoadBalancer loadBalancer;
    private final HostType hostType;

    HostFactoryImpl(LoadBalancer loadBalancer, HostType hostType) {
        this.loadBalancer = loadBalancer;
        this.hostType = hostType;
    }

    @Override
    public Host create(@NotNull Group group) {
        return new HostImpl(hostType, group, loadBalancer);
    }
}
