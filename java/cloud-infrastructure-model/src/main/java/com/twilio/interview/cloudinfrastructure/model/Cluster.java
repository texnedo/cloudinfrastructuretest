package com.twilio.interview.cloudinfrastructure.model;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.io.IOException;
import java.util.Collection;

public interface Cluster {
    void boot() throws IOException, IllegalStateException;

    void configure() throws IOException, IllegalStateException;

    void shutdown() throws IOException, IllegalStateException;

    void addHostType(@NotNull HostType hostType) throws IOException;

    void addGroupType(@NotNull GroupType groupType) throws IOException;

    void clearAllTypes() throws IOException;

    Collection<HostType> getHostTypes() throws IOException;

    Collection<GroupType> getGroupTypes() throws IOException;

    Group getGroupById(@NotNull String id) throws IOException;

    HostType getHostTypeById(@NotNull String id) throws IOException;

    GroupType getGroupTypeById(@NotNull String id) throws IOException;

    HostType getHostTypeByType(@NotNull String type) throws IOException;

    GroupType getGroupTypeByType(@NotNull String type) throws IOException;

    void addGroup(@NotNull GroupType groupType, @NotNull HostType hostType) throws IOException;

    void addGroup(@NotNull String groupType, @NotNull String hostType) throws IOException;

    void removeGroup(@NotNull Group group) throws IOException, IllegalStateException;

    void removeGroup(@NotNull String id) throws IOException, IllegalStateException;

    Collection<Group> getGroups();

    void activateGroup(@NotNull Group group) throws IllegalStateException, IOException;

    void deactivateGroup(@NotNull Group group) throws IOException, IllegalStateException;

    void activateGroup(@NotNull String id) throws IllegalStateException, IOException;

    void deactivateGroup(@NotNull String id) throws IOException, IllegalStateException;

    void activateGroupByType(@NotNull String type) throws IllegalStateException, IOException;

    void deactivateGroupByType(@NotNull String type) throws IOException, IllegalStateException;

    void activateHost(@NotNull Host host) throws IllegalStateException;

    void deactivateHost(@NotNull Host host) throws IllegalStateException;

    void activateHost(@NotNull String id) throws IllegalStateException;

    void deactivateHost(@NotNull String id) throws IllegalStateException;

    void onRequest(@NotNull String type,
                   @NotNull String request,
                   @Nullable LoadBalancer.ResultListener listener) throws IllegalStateException;

    void printConfig() throws IOException;

    void printGroups() throws IOException;
}
