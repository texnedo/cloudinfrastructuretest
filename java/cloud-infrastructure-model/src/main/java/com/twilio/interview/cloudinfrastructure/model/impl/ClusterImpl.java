package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.twilio.interview.cloudinfrastructure.model.*;
import com.twilio.interview.cloudinfrastructure.model.utils.TextUtils;

import java.io.IOException;
import java.util.*;

public class ClusterImpl implements Cluster {
    private final static String HOST_TYPE_STORAGE_FILE = "host_types.dat";
    private final static String GROUP_TYPE_STORAGE_FILE = "group_types.dat";
    private final TypeStorage<HostType> hostTypeStorage =
            new TypeStorageImpl<>(
                    HOST_TYPE_STORAGE_FILE,
                    new HostTypeStorageFactoryImpl()
            );
    private final TypeStorage<GroupType> groupTypeStorage =
            new TypeStorageImpl<>(
                    GROUP_TYPE_STORAGE_FILE,
                    new GroupTypeStorageFactoryImpl()
            );
    private final LoadBalancer loadBalancer = new LoadBalancerImpl();
    private final HashMap<String, Group> hostGroups = new HashMap<>();
    private final HashMap<String, HashSet<Group>> hostGroupsByType = new HashMap<>();
    private boolean isRunning = false;

    @Override
    public void boot() throws IOException, IllegalStateException {
        if (isRunning) {
            throw new IllegalStateException("The cluster has been already booted");
        }
        GroupDependencyResolver resolver =
                new GroupDependencyResolver(groupTypeStorage.get());
        List<GroupType> bootOrder = resolver.getBootOrder();
        System.out.println("boot plan: " + Arrays.toString(bootOrder.toArray()));
        for (GroupType groupType : bootOrder) {
            HashSet<Group> groups = hostGroupsByType.get(groupType.getType());
            if (groups == null || groups.isEmpty()) {
                throw new IllegalStateException(
                        String.format("Group of type %s must be added to the cluster",
                                groupType.getType()
                        )
                );
            }
            for (Group group : groups) {
                group.boot();
            }
        }
        isRunning = true;
    }

    @Override
    public void configure() throws IOException, IllegalStateException {
        if (isRunning) {
            throw new IllegalStateException("Configuration must be carried out before while the cluster is not running");
        }
        if (!hostGroups.isEmpty()) {
            throw new IllegalStateException("Configuration has been already made");
        }
        for(GroupType groupType : groupTypeStorage.get()) {
            addGroup(groupType, hostTypeStorage.getByType(groupType.getType()));
        }
    }

    @Override
    public void shutdown() throws IOException {
        if (!isRunning) {
            return;
        }
        GroupDependencyResolver resolver =
                new GroupDependencyResolver(groupTypeStorage.get());
        List<GroupType> shutdownOrder = resolver.getBootOrder();
        Collections.reverse(shutdownOrder);
        System.out.println("shutdown plan: " + Arrays.toString(shutdownOrder.toArray()));
        for (GroupType groupType : shutdownOrder) {
            HashSet<Group> groups = hostGroupsByType.get(groupType.getType());
            if (groups != null) {
                for (Group group : groups) {
                    group.shutdown();
                }
            }
        }
        isRunning = false;
    }

    @Override
    public void addHostType(@NotNull HostType hostType) throws IOException {
        hostTypeStorage.add(hostType);
    }

    @Override
    public void addGroupType(@NotNull GroupType groupType) throws IOException {
        groupTypeStorage.add(groupType);
    }

    @Override
    public void clearAllTypes() throws IOException {
        groupTypeStorage.clear();
        hostTypeStorage.clear();
    }

    @Override
    public Collection<HostType> getHostTypes() throws IOException {
        return hostTypeStorage.get();
    }

    @Override
    public Collection<GroupType> getGroupTypes() throws IOException {
        return groupTypeStorage.get();
    }

    @Override
    public Group getGroupById(@NotNull String id) throws IOException {
        return hostGroups.get(id);
    }

    @Override
    public HostType getHostTypeById(@NotNull String id) throws IOException {
        return hostTypeStorage.get(id);
    }

    @Override
    public GroupType getGroupTypeById(@NotNull String id) throws IOException {
        return groupTypeStorage.get(id);
    }

    @Override
    public HostType getHostTypeByType(@NotNull String type) throws IOException {
        return hostTypeStorage.getByType(type);
    }

    @Override
    public GroupType getGroupTypeByType(@NotNull String type) throws IOException {
        return groupTypeStorage.getByType(type);
    }

    @Override
    public void addGroup(@NotNull GroupType groupType,
                         @NotNull HostType hostType) throws IOException {
        Group group = new GroupImpl(groupType, new HostFactoryImpl(loadBalancer, hostType));
        //check group dependencies in case when cluster has already been started
        //this is necessary to support hot removal/adding
        if (isRunning) {
            checkDependenciesBeforeAdd(groupType);
        }
        hostGroups.put(group.getId(), group);
        HashSet<Group> groups = hostGroupsByType.get(group.getType());
        if (groups == null) {
            groups = new HashSet<>();
            hostGroupsByType.put(group.getType(), groups);
        }
        groups.add(group);
        System.out.println(
                String.format(
                        "Group %s (%s) has been added to the cluster",
                        group.getId(),
                        group.getType()
                )
        );
        if (isRunning) {
            group.boot();
        }
    }

    @Override
    public void addGroup(@NotNull String groupType,
                         @NotNull String hostType) throws IOException {
        GroupType groupTypeObj = groupTypeStorage.getByType(groupType);
        HostType hostTypeObj = hostTypeStorage.getByType(hostType);
        addGroup(groupTypeObj, hostTypeObj);
    }

    @Override
    public void removeGroup(@NotNull Group group) throws IOException {
        //check group dependencies in case when cluster has already been started
        //this is necessary to support hot removal/adding
        if (isRunning) {
            checkDependencyBeforeRemove(group.getType());
        }
        group.shutdown();
        hostGroups.remove(group.getId());
        hostGroupsByType.get(group.getType()).remove(group);
        System.out.println(
                String.format(
                    "Group %s (%s) has been removed from the cluster",
                    group.getId(),
                    group.getType()
            )
        );
    }

    @Override
    public void removeGroup(@NotNull String id) throws IOException {
        Group group = hostGroups.get(id);
        if (group != null) {
            removeGroup(group);
        }
    }

    @Override
    public Collection<Group> getGroups() {
        return hostGroups.values();
    }

    @Override
    public void activateGroup(@NotNull Group group) throws IllegalStateException, IOException {
        checkDependenciesBeforeActivation(group.getType());
        group.activate();
    }

    @Override
    public void deactivateGroup(@NotNull Group group) throws IllegalStateException, IOException {
        checkDependencyBeforeRemove(group.getType());
        group.deactivate();
    }

    @Override
    public void activateGroup(@NotNull String id) throws IllegalStateException, IOException {
        Group group = hostGroups.get(id);
        if (group != null) {
            checkDependenciesBeforeActivation(group.getType());
            group.activate();
        }
    }

    @Override
    public void deactivateGroup(@NotNull String id) throws IllegalStateException, IOException {
        Group group = hostGroups.get(id);
        if (group != null) {
            checkDependencyBeforeRemove(group.getType());
            group.deactivate();
        }
    }

    @Override
    public void activateGroupByType(@NotNull String type) throws IllegalStateException, IOException {
        HashSet<Group> groups = hostGroupsByType.get(type);
        if (groups != null) {
            checkDependenciesBeforeActivation(type);
            for (Group group : groups) {
                group.activate();
            }
        }
    }

    @Override
    public void deactivateGroupByType(@NotNull String type) throws IllegalStateException, IOException {
        checkDependencyBeforeRemove(type);
        HashSet<Group> groups = hostGroupsByType.get(type);
        if (groups != null) {
            for (Group group : groups) {
                group.deactivate();
            }
        }
    }

    @Override
    public void activateHost(@NotNull Host host) throws IllegalStateException {
        loadBalancer.addHost(host);
    }

    @Override
    public void deactivateHost(@NotNull Host host) throws IllegalStateException {
        loadBalancer.removeHost(host);
    }

    @Override
    public void activateHost(@NotNull String id) throws IllegalStateException {
        for (Group group : hostGroups.values()) {
            Host host = group.getHost(id);
            if (host != null) {
                loadBalancer.addHost(host);
            }
        }
    }

    @Override
    public void deactivateHost(@NotNull String id) throws IllegalStateException {
        loadBalancer.removeHost(id);
    }

    @Override
    public void onRequest(@NotNull String type,
                          @NotNull String request,
                          @Nullable LoadBalancer.ResultListener listener)
            throws IllegalStateException {
        loadBalancer.onRequest(type, request, listener);
    }

    @Override
    public void printConfig() throws IOException {
        System.out.println("Current cluster configuration");
        Cluster cluster1 = new ClusterImpl();
        for (HostType hostType : cluster1.getHostTypes()) {
            System.out.println(String.format("Host type %s (%s)", hostType.getId(), hostType.getType()));
        }
        for (GroupType groupType : cluster1.getGroupTypes()) {
            System.out.println(String.format("Group type %s (%s)", groupType.getId(), groupType.getType()));
        }
    }

    @Override
    public void printGroups() throws IOException {
        System.out.println("Current cluster groups");
        for (Group group : getGroups()) {
            System.out.println(group);
        }
    }

    private void checkDependenciesBeforeAdd(@NotNull GroupType groupType) {
        for (GroupType dependency : groupType.getDependencies()) {
            HashSet<Group> dependencyGroup = hostGroupsByType.get(dependency.getType());
            if (dependencyGroup == null || dependencyGroup.isEmpty()) {
                throw new IllegalStateException(
                        String.format(
                                "Group type %s (%s) must be added before group %s (%s) adding",
                                dependency.getId(),
                                dependency.getType(),
                                groupType.getId(),
                                groupType.getType()
                        )
                );
            }
        }
    }

    private void checkDependenciesBeforeActivation(@NotNull String groupTypeToActivate) throws IOException {
        GroupType groupType = groupTypeStorage.getByType(groupTypeToActivate);
        for (GroupType dependency : groupType.getDependencies()) {
            HashSet<Group> dependencyGroup = hostGroupsByType.get(dependency.getType());
            if (dependencyGroup == null || dependencyGroup.isEmpty()) {
                throw new IllegalStateException(
                        String.format(
                                "Group type %s (%s) must be added before group %s (%s) adding",
                                dependency.getId(),
                                dependency.getType(),
                                groupType.getId(),
                                groupType.getType()
                        )
                );
            }
            boolean foundOneActive = false;
            for (Group group : dependencyGroup) {
                foundOneActive |= group.isActive();
            }
            if (!foundOneActive) {
                throw new IllegalStateException(
                        String.format(
                                "Group type %s (%s) must be activated before group %s (%s) activation",
                                dependency.getId(),
                                dependency.getType(),
                                groupType.getId(),
                                groupType.getType()
                        )
                );
            }
        }
    }

    private void checkDependencyBeforeRemove(@NotNull String groupTypeToRemove) throws IOException {
        HashSet<Group> groups = hostGroupsByType.get(groupTypeToRemove);
        GroupDependencyResolver resolver =
                new GroupDependencyResolver(groupTypeStorage.get());
        List<GroupType> bootOrder = resolver.getBootOrder();
        if (bootOrder.isEmpty()) {
            throw new IllegalStateException(
                    String.format(
                            "Group type %s must be registered before group remove",
                            groupTypeToRemove
                    )
            );
        }
        ListIterator<GroupType> iterator = bootOrder.listIterator();
        GroupType currentGroupType = null;
        while (iterator.hasNext()) {
            GroupType groupType = iterator.next();
            //if some following groupTypes depends on the removing group groupType
            //and there is only one group of such groupType, raise an error
            if (currentGroupType != null &&
                    groupType.hasDependency(currentGroupType.getId()) &&
                    groups.size() == 1) {
                throw new IllegalStateException(
                        String.format(
                                "All groups with type %s (%s) must be removed before removing %s (%s)",
                                groupType.getType(),
                                groupType.getId(),
                                currentGroupType.getType(),
                                currentGroupType.getId()
                        )
                );
            }
            if (TextUtils.equals(groupType.getType(), groupTypeToRemove)) {
                currentGroupType = groupType;
            }

        }
    }
}
