package com.twilio.interview.cloudinfrastructure.model.impl;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import com.twilio.interview.cloudinfrastructure.model.Host;
import com.twilio.interview.cloudinfrastructure.model.LoadBalancer;
import com.twilio.interview.cloudinfrastructure.model.utils.RoundRobbinLinkedList;

import java.util.HashMap;

class LoadBalancerImpl implements LoadBalancer {
    private HashMap<String, Host> activeHosts = new HashMap<>();
    private HashMap<String, RoundRobbinLinkedList<Host>> activeHostsByType = new HashMap<>();

    @Override
    public void onRequest(@NotNull String type,
                          @NotNull String request,
                          @Nullable ResultListener listener) {
        RoundRobbinLinkedList<Host> hosts = activeHostsByType.get(type);
        if (hosts == null || hosts.isEmpty()) {
            throw new IllegalStateException("No host to serve request with type " + type);
        }
        Host nextHost = hosts.next();
        if (nextHost == null) {
            throw new IllegalStateException("List must contain at least one host");
        }
        System.out.println(
                String.format(
                        "Request %s (%s) will be processed on host %s (%s)",
                        request,
                        type,
                        nextHost.getId(),
                        nextHost.getType()
                )
        );
        nextHost.onRequest(type, request, listener);
    }

    @Override
    public void addHost(@NotNull Host host) throws IllegalStateException {
        if (activeHosts.put(host.getId(), host) == null) {
            addToHostByTypeIndex(host);
        }
    }

    @Override
    public void removeHost(@NotNull Host host) throws IllegalStateException {
        activeHosts.remove(host.getId());
        removeFromHostByTypeIndex(host);
    }

    @Override
    public void removeHost(@NotNull String id) throws IllegalStateException {
        Host host = activeHosts.remove(id);
        if (host != null) {
            removeFromHostByTypeIndex(host);
        }
    }

    @Override
    public boolean isActive(@NotNull Host host) {
        return activeHosts.containsKey(host.getId());
    }

    @Override
    public boolean isActive(@NotNull String id) {
        return activeHosts.containsKey(id);
    }

    private void addToHostByTypeIndex(@NotNull Host host) {
        RoundRobbinLinkedList<Host> hosts = activeHostsByType.get(host.getType());
        if (hosts == null) {
            hosts = new RoundRobbinLinkedList<>();
            activeHostsByType.put(host.getType(), hosts);
        }
        hosts.add(host);
        System.out.println(
                String.format(
                        "Host %s (%s) has been brought to balancer",
                        host.getId(),
                        host.getType()
                )
        );
    }

    private void removeFromHostByTypeIndex(Host host) {
        RoundRobbinLinkedList<Host> hosts = activeHostsByType.get(host.getType());
        //slow operation on a host removal, but it should be relatively rare
        hosts.remove(host);
        System.out.println(
                String.format(
                        "Host %s (%s) has been removed from balancer",
                        host.getId(),
                        host.getType()
                )
        );
    }
}
