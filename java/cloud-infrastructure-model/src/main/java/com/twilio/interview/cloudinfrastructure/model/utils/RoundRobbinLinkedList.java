package com.twilio.interview.cloudinfrastructure.model.utils;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

public class RoundRobbinLinkedList<T> {
    private Node<T> head;
    private Node<T> tail;
    private Node<T> iterator;

    public boolean isEmpty() {
        return head == null;
    }

    public void add(@NotNull T item) {
        if (head == null) {
            head = new Node<>(item);
            tail = head;
        } else {
            tail.next = new Node<>(item);
            tail = tail.next;
        }
    }

    public void remove(@NotNull T item) {
        Node<T> start = head;
        Node<T> previous = null;
        while (start != null) {
            if (start.value == item) {
                break;
            }
            previous = start;
            start = start.next;
        }
        if (start != null) {
            if (start == head) {
                head = head.next;
            } else if (start == tail){
                tail = previous;
            } else {
                previous.next = start.next;
            }
            if (iterator == start) {
                iterator = iterator.next;
            }
        }
    }

    @Nullable
    public T next() {
        if (iterator == null) {
            iterator = head;
        }
        if (iterator == null) {
            return null;
        }
        T current = iterator.value;
        iterator = iterator.next;
        return current;
    }

    private static class Node<T> {
        T value;
        Node<T> next;

        Node(@NotNull T item) {
            this.value = item;
        }
    }
}
