package com.twilio.interview.cloudinfrastructure.shell;

import com.sun.istack.internal.NotNull;
import com.twilio.interview.cloudinfrastructure.model.*;
import com.twilio.interview.cloudinfrastructure.model.impl.ClusterImpl;
import com.twilio.interview.cloudinfrastructure.model.impl.GroupTypeImpl;
import com.twilio.interview.cloudinfrastructure.model.impl.HostTypeImpl;
import com.twilio.interview.cloudinfrastructure.model.utils.TextUtils;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.*;

class ClusterCli {
    void run(@NotNull String[] args) throws IOException {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption("b", "boot", false, "Boot the cluster with last saved configuration");
        options.addOption("p", "print", false, "Print last save cluster configuration");
        options.addOption("r", "reset", false, "Reset last save cluster configuration");
        options.addOption("s", "shell", false, "Show command shell for the cluster");
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Cluster command line utility", options);

        try {
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("boot")) {
                Cluster cluster = new ClusterImpl();
                cluster.configure();
                cluster.boot();
            } else if (line.hasOption("print")) {
                Cluster cluster = new ClusterImpl();
                cluster.printConfig();
            } else if (line.hasOption("reset")) {
                Cluster cluster = new ClusterImpl();
                cluster.clearAllTypes();
            } else if (line.hasOption("shell")) {
                Options shellOptions = new Options();
                shellOptions.addOption("exit", false, "Exit the cluster shell");
                shellOptions.addOption("sample", false, "Generate sample cluster configuration");
                shellOptions.addOption("help", false, "Print available shell commands");
                shellOptions.addOption("reset", false, "Reset last save cluster configuration");
                shellOptions.addOption("print", false, "Print last save cluster configuration");
                shellOptions.addOption("add", false, "Add entity");
                shellOptions.addOption("remove", false, "Remove entity");
                shellOptions.addOption("edit", false, "Edit entity");
                shellOptions.addOption("boot", false, "Boot the cluster");
                shellOptions.addOption("shutdown", false, "Shutdown the cluster");
                shellOptions.addOption("process", false, "Process request");
                shellOptions.addOption("type", true, "Request type");
                shellOptions.addOption("request", true, "Request body");
                shellOptions.addOption("balancer", false, "Edit load balancer configuration");
                HelpFormatter formatterShell = new HelpFormatter();
                formatterShell.printHelp("Cluster shell", shellOptions);
                Cluster cluster = new ClusterImpl();
                while (true) {
                    try {
                        CommandLineParser shellParser = new DefaultParser();
                        System.out.print(">");
                        Scanner scanner = new Scanner(System.in);
                        String[] shellArgs = fixCommand(scanner.nextLine().split(" "), shellOptions);
                        if (shellArgs.length == 0) {
                            continue;
                        }
                        CommandLine shellLine = shellParser.parse(shellOptions, shellArgs);
                        if (shellLine.hasOption("exit")) {
                            break;
                        } else if (shellLine.hasOption("help")) {
                            formatterShell.printHelp("Cluster shell", shellOptions);
                        } else if (shellLine.hasOption("reset")) {
                            cluster.clearAllTypes();
                        } else if (shellLine.hasOption("print")) {
                            cluster.printConfig();
                        } else if (shellLine.hasOption("add")) {
                            addEntity(cluster);
                        } else if (shellLine.hasOption("remove")) {
                            removeEntity(cluster);
                        } else if (shellLine.hasOption("edit")) {
                            editEntity(cluster);
                        } else if (shellLine.hasOption("shutdown")) {
                            cluster.shutdown();
                        } else if (shellLine.hasOption("boot")) {
                            cluster.configure();
                            cluster.boot();
                        } else if (shellLine.hasOption("process")) {
                            cluster.onRequest(
                                    shellLine.getOptionValue("type"),
                                    shellLine.getOptionValue("request"),
                                    new LoadBalancer.ResultListener() {
                                        @Override
                                        public void onResult(@NotNull String result) {
                                            System.out.println(
                                                    String.format("Request %s with type %s, result %s",
                                                            shellLine.getOptionValue("request"),
                                                            shellLine.getOptionValue("type"),
                                                            result
                                                    )
                                            );
                                        }

                                        @Override
                                        public void onError(LoadBalancer.ErrorType errorType) {

                                        }
                                    }
                            );
                        } else if (shellLine.hasOption("sample")) {
                            makeSampleConfig();
                        } else if (shellLine.hasOption("balancer")) {
                            configureBalancer(cluster);
                        }
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                throw new IllegalArgumentException("Wrong option provided");
            }
        }
        catch(ParseException ex) {
            System.out.println("Unexpected exception:" + ex.getMessage());
        }
    }

    private void configureBalancer(@NotNull Cluster cluster) throws ParseException, IOException {
        Options shellOptions = new Options();
        shellOptions.addOption("addGroup", false, "Add group to balancer");
        shellOptions.addOption("removeGroup", false, "Remove group from balancer");
        shellOptions.addOption("addHost", false, "Add host to balancer");
        shellOptions.addOption("removeHost", false, "Remove host from balancer");
        shellOptions.addOption("id", true, "");
        shellOptions.addOption("type", true, "");
        shellOptions.addOption("break", false, "Return to the upper level");
        HelpFormatter formatterShell = new HelpFormatter();
        formatterShell.printHelp("Configure balancer", shellOptions);
        CommandLineParser shellParser = new DefaultParser();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("configureBalancer>>");
            String[] shellArgs = fixCommand(scanner.nextLine().split(" "), shellOptions);
            if (shellArgs.length == 0) {
                continue;
            }
            CommandLine shellLine = shellParser.parse(shellOptions, shellArgs);
            if (shellLine.hasOption("break")) {
                break;
            }
            if (shellLine.hasOption("addGroup") && shellLine.hasOption("id")) {
                cluster.activateGroup(shellLine.getOptionValue("id"));
            }
            if (shellLine.hasOption("removeGroup") && shellLine.hasOption("id")) {
                cluster.deactivateGroup(shellLine.getOptionValue("id"));
            }
            if (shellLine.hasOption("addHost") && shellLine.hasOption("id")) {
                cluster.activateHost(shellLine.getOptionValue("id"));
            }
            if (shellLine.hasOption("removeHost") && shellLine.hasOption("id")) {
                cluster.deactivateHost(shellLine.getOptionValue("id"));
            }
            if (shellLine.hasOption("addGroup") && shellLine.hasOption("type")) {
                cluster.activateGroupByType(shellLine.getOptionValue("type"));
            }
            if (shellLine.hasOption("removeGroup") && shellLine.hasOption("type")) {
                cluster.deactivateGroupByType(shellLine.getOptionValue("type"));
            }
        }
    }

    private void editEntity(@NotNull Cluster cluster) throws ParseException, IOException {
        Options shellOptions = new Options();
        shellOptions.addOption("groupType", false, "Edit group type");
        shellOptions.addOption("hostType", false, "Edit host type");
        shellOptions.addOption("id", true, "");
        shellOptions.addOption("type", true, "");
        shellOptions.addOption("description", true, "");
        shellOptions.addOption("dependencies", true, "");
        shellOptions.addOption("break", false, "Return to the upper level");
        HelpFormatter formatterShell = new HelpFormatter();
        formatterShell.printHelp("Edit entity", shellOptions);
        CommandLineParser shellParser = new DefaultParser();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("editEntity>>");
            String[] shellArgs = fixCommand(scanner.nextLine().split(" "), shellOptions);
            if (shellArgs.length == 0) {
                continue;
            }
            CommandLine shellLine = shellParser.parse(shellOptions, shellArgs);
            if (shellLine.hasOption("break")) {
                break;
            }
            if (!shellLine.hasOption("id") && !shellLine.hasOption("description")) {
                continue;
            }
            if (shellLine.hasOption("groupType")) {
                GroupType groupType = null;
                if (shellLine.hasOption("id")) {
                    groupType = cluster.getGroupTypeById(shellLine.getOptionValue("id"));
                }
                if (shellLine.hasOption("type")) {
                    groupType = cluster.getGroupTypeByType(shellLine.getOptionValue("type"));
                }
                if (groupType != null && shellLine.hasOption("description")) {
                    groupType.setDescription(shellLine.getOptionValue("description"));
                }
                if (groupType != null && shellLine.hasOption("dependencies")) {
                    String[] dependencies = shellLine.getOptionValues("dependencies");
                    for (String item : dependencies) {
                        groupType.addDependency(
                                cluster.getGroupTypeByType(item)
                        );
                    }
                }
                cluster.printConfig();
            } else if (shellLine.hasOption("hostType") &&
                    shellLine.hasOption("description")) {
                if (shellLine.hasOption("id")) {
                    cluster.getHostTypeById(shellLine.getOptionValue("id"))
                            .setDescription(shellLine.getOptionValue("description"));
                }
                if (shellLine.hasOption("type")) {
                    cluster.getHostTypeByType(shellLine.getOptionValue("type"))
                            .setDescription(shellLine.getOptionValue("description"));
                }
                cluster.printConfig();
            }
        }
    }

    private void removeEntity(@NotNull Cluster cluster) throws ParseException, IOException {
        Options shellOptions = new Options();
        shellOptions.addOption("groupType", false, "Remove group type");
        shellOptions.addOption("hostType", false, "Remove host type");
        shellOptions.addOption("id", true, "");
        shellOptions.addOption("type", true, "");
        shellOptions.addOption("break", false, "Return to the upper level");
        HelpFormatter formatterShell = new HelpFormatter();
        formatterShell.printHelp("Remove entity", shellOptions);
        CommandLineParser shellParser = new DefaultParser();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("removeEntity>>");
            String[] shellArgs = fixCommand(scanner.nextLine().split(" "), shellOptions);
            if (shellArgs.length == 0) {
                continue;
            }
            CommandLine shellLine = shellParser.parse(shellOptions, shellArgs);
            if (shellLine.hasOption("break")) {
                break;
            }
            if (!shellLine.hasOption("id") && !shellLine.hasOption("description")) {
                continue;
            }
            if (shellLine.hasOption("groupType")) {
                if (shellLine.hasOption("id")) {
                    cluster.getGroupTypeById(shellLine.getOptionValue("id")).remove();
                }
                if (shellLine.hasOption("type")) {
                    cluster.getGroupTypeByType(shellLine.getOptionValue("type")).remove();
                }
                cluster.printConfig();
            } else if (shellLine.hasOption("hostType")) {
                if (shellLine.hasOption("id")) {
                    cluster.getHostTypeById(shellLine.getOptionValue("id")).remove();
                }
                if (shellLine.hasOption("type")) {
                    cluster.getHostTypeByType(shellLine.getOptionValue("type")).remove();
                }
                cluster.printConfig();
            }
        }
    }

    private void addEntity(@NotNull Cluster cluster) throws ParseException, IOException {
        Options shellOptions = new Options();
        shellOptions.addOption("groupType", false, "Add group type");
        shellOptions.addOption("hostType", false, "Add host type");
        shellOptions.addOption("group", false, "Add host group");
        shellOptions.addOption("type", true, "");
        shellOptions.addOption("host", true, "");
        shellOptions.addOption("description", true, "");
        shellOptions.addOption("hostCount", true, "");
        shellOptions.addOption("hostSize", true, Arrays.toString(HostSize.values()));
        shellOptions.addOption("isActiveOnBoot", true, "");
        shellOptions.addOption("break", false, "Return to the upper level");
        HelpFormatter formatterShell = new HelpFormatter();
        formatterShell.printHelp("Add entity", shellOptions);
        CommandLineParser shellParser = new DefaultParser();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("addEntity>>");
            String[] shellArgs = fixCommand(scanner.nextLine().split(" "), shellOptions);
            if (shellArgs.length == 0) {
                continue;
            }
            CommandLine shellLine = shellParser.parse(shellOptions, shellArgs);
            if (shellLine.hasOption("break")) {
                break;
            }
            if (shellLine.hasOption("groupType") &&
                    shellLine.hasOption("type") &&
                    shellLine.hasOption("description")) {
                cluster.addGroupType(new GroupTypeImpl(
                        shellLine.getOptionValue("type"),
                        shellLine.getOptionValue("description"),
                        shellLine.hasOption("hostSize") ? HostSize.valueOf(shellLine.getOptionValue("hostSize")) : HostSize.MEDIUM,
                        shellLine.hasOption("hostCount") ? Integer.parseInt(shellLine.getOptionValue("hostCount")) : 5,
                        !shellLine.hasOption("isActiveOnBoot") || Boolean.parseBoolean(shellLine.getOptionValue("isActiveOnBoot"))
                ));
                cluster.printConfig();
            } else if (shellLine.hasOption("hostType") &&
                    shellLine.hasOption("type") &&
                    shellLine.hasOption("description")) {
                cluster.addHostType(new HostTypeImpl(
                        shellLine.getOptionValue("type"),
                        shellLine.getOptionValue("description")
                ));
                cluster.printConfig();
            } else if (shellLine.hasOption("group") &&
                    shellLine.hasOption("type") &&
                    shellLine.hasOption("host")) {
                cluster.addGroup(
                        shellLine.getOptionValue("type"),
                        shellLine.getOptionValue("host")
                );
                cluster.printGroups();
            }
        }
    }

    private static void makeSampleConfig() throws IOException {
        Cluster cluster = new ClusterImpl();
        cluster.clearAllTypes();
        HostType hostAuth = new HostTypeImpl("auth", "");
        HostType hostDbQueries = new HostTypeImpl("db-queries", "");
        HostType hostMessages = new HostTypeImpl("messages", "");
        HostType hostPymk = new HostTypeImpl("pymk", "");
        HostType hostContactList = new HostTypeImpl("contact-list", "");
        cluster.addHostType(hostAuth);
        cluster.addHostType(hostDbQueries);
        cluster.addHostType(hostMessages);
        cluster.addHostType(hostPymk);
        cluster.addHostType(hostContactList);
        GroupType auth = new GroupTypeImpl("auth", "Handles user authorization", HostSize.MEDIUM, 2, true);
        GroupType dbQueries = new GroupTypeImpl("db-queries", "Processes queries to db", HostSize.LARGE, 5, true);
        GroupType messages = new GroupTypeImpl("messages", "Processes messages from one user to another", HostSize.LARGE, 7, true);
        GroupType pymk = new GroupTypeImpl("pymk", "Processes people you may know requests", HostSize.LARGE, 4, true);
        GroupType contactList = new GroupTypeImpl("contact-list", "Processes contact list requests", HostSize.LARGE, 3, true);
        cluster.addGroupType(auth);
        cluster.addGroupType(dbQueries);
        cluster.addGroupType(messages);
        cluster.addGroupType(pymk);
        cluster.addGroupType(contactList);
        messages.addDependency(auth);
        messages.addDependency(contactList);
        messages.addDependency(dbQueries);
        pymk.addDependency(contactList);
        contactList.addDependency(dbQueries);
    }

    private static String[] fixCommand(@NotNull String[] input,
                                       @NotNull Options allowedCommands) {
        if (input.length == 0) {
            return input;
        }
        for (int i = 0; i < input.length; ++i) {
            String token = input[i];
            if (!TextUtils.isEmpty(token) &&
                    token.charAt(0) != '-' &&
                    allowedCommands.hasOption(token)) {
                input[i] = "-" + token;
            }
        }
        return input;
    }
}
