package com.twilio.interview.cloudinfrastructure.shell;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ClusterCli cli = new ClusterCli();
        cli.run(args);
    }
}
